import urllib.request

from django.apps import AppConfig
from .channel import YTChannel
from . import data

class YouTubeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'youtubeXML'
    def ready(self):
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + 'UC300utwSVAYOoRLEqmsprfg'
        xml = urllib.request.urlopen(url)
        channel = YTChannel(xml)
        data.selectable = channel.videos()