from django.db import models

# Create your models here.

class Video(models.Model):
    url = models.URLField()
    title = models.CharField(max_length=200)
    choosen = models.BooleanField(default=False)